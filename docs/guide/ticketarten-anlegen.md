# Ticket-Arten anlegen
Jedes Event kann über mehrere Ticket-Arten verfügen, beispielsweise **************************Erwachsen************************** oder **********Schüler**********.
Für jeden Ticket-Typ können Limit, Preis und Verfügbarkeit festgelegt werden. So kann ein Ticket auch manuell oder nach Erreichen des Limits deaktiviert werden.
Durch Verwendung der Einladungs-Gruppen können Einladungen auf selektierte Ticket-Arten beschränkt werden.

:::details Beispiel
| Name | Limit | Preis | Aktiv |
| --- | --- | --- | --- |
| Abiball & Afterparty | 300 | 35€ | Ja |
| Nur Afterparty | 500 | 15€ | Ja |
:::

Um nun eine Ticket-Art anzulegen, fülle das rechts angezeigte Formular aus und klicke auf Ticket-Typ anlegen; zum Bearbeiten klickst du einfach auf das ✎-Symbol am Ende einer Ticket-Arten-Zeile hin.