# Event anlegen
Um ein Event anzulegen, logge dich zunächst unter [https://edutix.de](https://edutix.de) in deinen Account ein. Auf der Event-Übersicht klickst du nun oben rechts auf **Event anlegen** und füllst die gefragten Felder aus. Nach Klicken auf **Event erstellen** kann es bis zu 1 Minute dauern, während dein Event für Dich erstellt und vorbereitet wird.

:::tip
Du kannst alle Angaben nach der Erstellung noch nach Belieben anpassen.
:::

Nachdem das Event angelegt wurde, wirst du auf die Detail-Seite weitergeleitet. Dort kannst du zunächst deine
Event-Details anpassen, Bankdaten angeben, an die die Ticket-Zahlungen überwiesen werden sollen (siehe [Bezahlung](#)).