---
layout: home

hero:
  name: "eduTix"
  tagline: "smartes Ticketing für Schüler & Schulen."
  text: 'Hilfe & Support'
  actions:
    - theme: brand
      link: "/guide/"
      text: "Los geht's!"
    - theme: alt
      text: Zum eduTix Veranstalter-Login
      link: https://edutix.de/login
  image:
    src: /web_banner.png
    alt: eduTix Logo

features:
  - icon: "🚀"
    title: Schnell & einfach
    details: Einrichten ohne Aufwand und ohne Programmierkenntnisse. Einfach loslegen und losverkaufen.
    link: "/guide/"
  - icon: "🔥"
    title: Smartes & Modernes Design
    details: Ein modernes Design, das sich an den Bedürfnissen von Schülern und Schulen orientiert.
    link: "https://edutix.de"
  - icon: "💬"
    title: Persönlicher Support
    details: Wir helfen dir gerne bei allen Fragen rund um eduTix. Starte einfach einen Chat und stelle Deine Frage!
    link: "https://edutix.de"
---