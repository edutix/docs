import {defineConfig} from "vitepress";


export default defineConfig({
    lang: "de-DE",
    title: 'eduTix Support',
    description: 'Das smarte Ticketing System für Schulen',
    cleanUrls: true,
    lastUpdated: true,
    themeConfig: {
        footer: {
            copyright: '© 2023 eduTix',
        },
        returnToTopLabel: 'Nach oben',
        lastUpdatedText: 'Letzte Änderung',
        docFooter: {
            prev: 'Letzte Seite',
            next: 'Nächste Seite'
        },
        siteTitle: 'eduTix Support',
        nav: [
            { text: 'Los geht\'s', link: '/guide/' },
            { text: 'Zu eduTix', link: 'https://edutix.de/' },
        ],
        sidebar: {
            '/guide/': [
                {
                    text: 'Erste Schritte',
                    collapsed: false,
                    items: [
                        { text: 'Über diesen Guide', link: '/guide/' },
                        { text: 'Event anlegen', link: '/guide/event-anlegen' },
                        { text: 'Ticket-Arten anlegen', link: '/guide/ticketarten-anlegen' },
                    ]
                }
            ]
        },
    },
    locales: {
        "root": {
            label: "Deutsch",
            lang: 'de',
            title: 'eduTix Support',
        }
    },
    head: [
        [
          "script",
            {
                src: "https://identity.netlify.com/v1/netlify-identity-widget.js"
            }
        ]
    ]
})